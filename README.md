Assimp Collada Importer issue on s390x
======================================

This is a minimal example to reproduce Assimp loading error of CAE file on s390x platform.

To reproduce:

 - Clone this repository
 - create a `build` folder
 - in the build folder run `cmake ..`
 - then `cmake --build .`
 - and `./assimp_collada_test`

On amd64 the output is:

```
Info,  T0: Load cube.dae
Debug, T0: Assimp 5.2.0 amd64 gcc debug shared singlethreadedsingle : 
Info,  T0: Found a matching importer for this file format: Collada Importer.
Info,  T0: Import root directory is './'
Debug, T0: Collada schema version is 1.5.n
Debug, T0: UpdateImporterScale scale set: 1
Info,  T0: Entering post processing pipeline
Debug, T0: LimitBoneWeightsProcess begin
Debug, T0: LimitBoneWeightsProcess end
Info,  T0: Leaving post processing pipeline
Assimp import OK
```

On s390x, the output is:

```
Info,  T0: Load cube.dae
Debug, T0: Assimp 5.2.0 s390x gcc debug shared singlethreadedsingle : 
Info,  T0: Found a matching importer for this file format: Collada Importer.
Info,  T0: Import root directory is './'
Debug, T0: Collada schema version is 1.5.n
Error, T0: Expected different index count in <p> element.
Assimp error: Expected different index count in <p> element.
```
